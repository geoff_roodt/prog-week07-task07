﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_07
{
    class Program
    {
        static void Main(string[] args)
        {
            double numOne = 0;
            double numTwo = 0;
            double numThree = 0;
            double numFour = 0;
            double numFive = 0;

            Console.WriteLine("Enter numer 1:");
            numOne = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter numer 2:");
            numTwo = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter numer 3:");
            numThree = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter numer 4:");
            numFour = double.Parse(Console.ReadLine());

            Console.WriteLine("Enter numer 5:");
            numFive = double.Parse(Console.ReadLine());

            var sum = AddNumbers(numOne, numTwo, numThree, numFour, numFive);
            Console.WriteLine($"The sum of your numbers are: {sum}");
        }

        static double AddNumbers(double num1, double num2, double num3, double num4, double num5)
        {
            return (num1 + num2 + num3 + num4 + num5);
        }
    }
}
